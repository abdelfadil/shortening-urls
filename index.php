<!-- project_1 : site pour raccourcir les URL -->
<?php
	try {
		$app_projet_1 = new PDO('mysql:host=localhost;dbname=app_projet_1;charset=utf8','root','');
	} catch (Exception $e) {
		die("Erreur : ".$e->getMessage());
	}

	// - Verification, Raccourcissemment, et Envoie d'une URL dans la base de données app_projet_1
	if (isset($_POST['url'])) {
		$url = $_POST['url'];
		// - Verification URL
		if (!filter_var($url, FILTER_VALIDATE_URL)) {
			header('Location: ?error=true&message=Adresse URL non valide');
			exit();
		}else{
			// - Verifier si l'URL existe dans la base de donnees
			$select = $app_projet_1->prepare('SELECT COUNT(*) AS nbr FROM links WHERE url = ?');
			$select->execute(array($url)) or die(print_r($app_projet_1->errorInfo()));
			if (($select->fetch())['nbr'] != 0) {
				header('Location: ?error=true&message=Adresse URL déjà raccourcir');
				exit();
			}else{
				// - Raccourcir l'URL
				$shortcut = crypt($url, time());
				// - Envoie dans la base de données
				$insert = $app_projet_1->prepare('INSERT INTO links(url, shortcut) VALUES(?,?)');
				$insert->execute(array($url, $shortcut)) or die(print_r($app_projet_1->errorInfo()));
				header('Location: ?short='.$shortcut);
				exit();
			}
		}
	}

	// - Verification et Redirection d'un shortcut vers une URL
	if (isset($_GET['q'])) {
		$shortcut = htmlspecialchars($_GET['q']);
		$requet = $app_projet_1->prepare('SELECT COUNT(*) AS nbr FROM links WHERE shortcut = ?');
		$requet->execute(array($shortcut));
		// - Verification shortcut
		if (($requet->fetch())['nbr'] != 1) {
			header('Location: http://localhost/app_projet_1/index.php?error=true&message=Adresse URL non connue');
			exit();
		}else{
			// - Redirection de la shortcut
			$data = $app_projet_1->prepare('SELECT * FROM links WHERE shortcut = ?');
			$data->execute(array($shortcut));
			header('Location: '.($data->fetch())['url']);
			exit();
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>http://localhost/app_projet_1/index.php</title>
		<link rel='stylesheet' type='text/css' href='css/style.css'>
		<link rel='icon' type='image/png' href='img/lilly.png'>
	</head>
	<body>
		<section id='hero' class='hero'>
			<div class='container'>
				<header class='header'>
					<img src='img/lilly.png' alt='lilly' class='logo'>
				</header>
				<h1>Une URL longue ? Raccourcissez-là ?</h1>
				<h2>Largement meilleur et plus courte que les autres !</h2>
				<form method='POST' action='index.php'>
					<input type='url' name='url' placeholder='Collez le lien à raccourcir' required='' />
					<input type='submit' value='Raccourcir'>
				</form>
				<?php if (isset($_GET['error']) and isset($_GET['message'])) { ?>
					<div class='container'>
						<div class='result-form'>
							<b><?php echo htmlspecialchars($_GET['message']);  ?></b>
						</div>
					</div>
				<?php }elseif (isset($_GET['short'])) { ?>
					<div class='container'>
						<div class='result-form'>
							<b>URL RACCOURCIR :</b> http://localhost/?q=<?php echo htmlspecialchars($_GET['short']);  ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</section>
		<section id='brands'>
			<div class='container'>
				<h3>Ces marques nous font confiance</h3>
				<div class='container'>
					<img src='img/myob.png' alt='myob' class='img-fluid'>
					<img src='img/life-groups.png' alt='life-groups' class='img-fluid'>
					<img src='img/belimo.png' alt='belimo' class='img-fluid'>
					<img src='img/citrus.png' alt='citrus' class='img-fluid'>
				</div>
			</div>
		</section>
		<footer id='footer'>
			<div class='container'>
				<div class='container'>
					<img src='img/lilly.png' alt='l' class='img-fluid'>
					<div class='copyright'>&copy;copyright lilly 2022</div>
					<nav class='nav-link'><a href="">Contact</a> - <a href="">À propos</a></nav>
				</div>
		</footer>
	</body>
</html>